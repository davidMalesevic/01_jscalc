/**
 * Created by dave on 05/06/2017.
 */

function getHTMLdata(){
    var amount =  document.getElementById("currencyCalcInput").value;

    var b = document.getElementById("baseC");
    var baseUnit = b.options[b.selectedIndex].value;

    var t = document.getElementById("targetC");
    var targetUnit = t.options[t.selectedIndex].value;

    calcCurrency(amount, baseUnit, targetUnit);

}


function calcCurrency(amount, baseUnit, targetUnit){


    console.log(amount, baseUnit, targetUnit);

    const rates = {
        franken: {
            franken: 1,
            euro: 0.1,
            yen: 0.2,
            dollar: 0.3,
            yuan: 0.4
        },
        euro: {
            euro: 1,
            franken: 0.1,
            yen: 0.2,
            dollar: 0.3,
            yuan: 0.4
        },
        yen: {
            yen: 1,
            franken: 0.1,
            euro: 0.2,
            dollar: 0.3,
            yuan: 0.4
        },
        dollar: {
            dollar: 1,
            euro: 0.1,
            yen: 0.2,
            franken: 0.3,
            yuan: 0.4
        },
        yuan: {
            yuan: 1,
            euro: 0.1,
            yen: 0.2,
            dollar: 0.3,
            franken: 0.4
        }
    }

    var rate  = 0;

    var base = rates[baseUnit];
    if (!base){
        throw new Error(
            "Could not find " + baseUnit
        );
    }

    var rate = base[targetUnit];
    if (!rate){
        throw new Error(
            "Could not find" + targetUnit + "for base unit" + baseUnit
        );
    }

    result = amount * rate;


    document.getElementById("currOutput").innerHTML = result.toString();
}

